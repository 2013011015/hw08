# Makefile

all: sorted_array matrix

matrix: matrix.h matrix_main.cc
	g++ -o matrix matrix_main.cc

sorted_array: sorted_array.h sorted_array_main.cc
	g++ -o sorted_array sorted_array_main.cc

